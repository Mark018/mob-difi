package hu.mark.mobdifi;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Particle;
import org.bukkit.Statistic;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.binary.Base64;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftLivingEntity;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Bee;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedEnterEvent.BedEnterResult;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.dnyferguson.mineablespawners.MineableSpawners;
import com.dnyferguson.mineablespawners.api.API;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_16_R3.AttributeModifiable;
import net.minecraft.server.v1_16_R3.EntityInsentient;
import net.minecraft.server.v1_16_R3.GenericAttributes;
import su.nightexpress.goldenenchants.manager.EnchantManager;
import su.nightexpress.goldenenchants.manager.enchants.api.type.ObtainType;

public class MobDifi extends JavaPlugin implements Listener {
	private Random rand;
	private static World world;
	public static boolean skipping;
	public static Integer sleepers;
	
	private static ItemStack hpApple;
	
	public List<Totem> totems;
	
	public static List<Entity> lvl5mobs;
	public static List<Entity> lvl4mobs;
	public ItemStack lucky;
	
	private static ConfigManager config;
	public FileConfiguration totemConfig;
	public boolean damageevent;
	
	public void onEnable() {
		config = new ConfigManager("plugins/" + getName() + "/");
		totemConfig = config.getConfig("totems.yml");
		config.saveConfig("totems.yml");
		
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new TotemEvent(this), this);
		getServer().getPluginManager().registerEvents(new EnchantmentEvent(this), this);
		rand = new Random();
		sleepers = 0;
		skipping = false;
		lvl5mobs = new ArrayList<Entity>();
		lvl4mobs = new ArrayList<Entity>();
		
		totems = new ArrayList<Totem>();
		
		if (totemConfig.isSet("totems")) {
			for(String path : totemConfig.getConfigurationSection("totems").getKeys(false)) {
				try {
					totems.add(new Totem(new Location(Bukkit.getWorld(totemConfig.getString("totems." + path + ".world")), totemConfig.getDouble("totems." + path + ".x"), totemConfig.getDouble("totems." + path + ".y"), totemConfig.getDouble("totems." + path + ".z")), totemConfig.getInt("totems." + path + ".weight")));
				} catch(Exception ex) {
					
				}
			}
		}
		
		System.out.println("Loaded " + totems.size() + " totems!");
		
		hpApple = new ItemStack(Material.GOLDEN_APPLE, 1);
		ItemMeta meta = hpApple.getItemMeta();
		meta.setDisplayName("�4HP Alma");
		meta.setLore(Arrays.asList(new String[] {"�rN�velheted a maximum HP-dat.", "�rMaximum 40 �leted lehet."}));
		hpApple.setItemMeta(meta);
		NamespacedKey key = new NamespacedKey(this, "hp_apple");
		ShapedRecipe recipe = new ShapedRecipe(key, hpApple);
		recipe.shape("EEE", "ESE", "EEE");
		recipe.setIngredient('E', Material.EMERALD_BLOCK);
		recipe.setIngredient('S', Material.GOLDEN_APPLE);
		Bukkit.addRecipe(recipe);
		
		lucky = getHead("http://textures.minecraft.net/texture/519d28a8632fa4d87ca199bbc2e88cf368dedd55747017ae34843569f7a634c5");
		ItemMeta lumeta = lucky.getItemMeta();
		lumeta.setDisplayName("�6Lucky Block");
		lucky.setItemMeta(lumeta);
		
		API api = MineableSpawners.getApi();
		
		ItemStack spawner = api.getSpawnerFromEntityType(EntityType.ZOMBIE);
        NamespacedKey key2 = new NamespacedKey(this, "zombie_spawner");
		ShapedRecipe recipe2 = new ShapedRecipe(key2, spawner);
		recipe2.shape("DND", "DZD", "DDD");
		recipe2.setIngredient('D', Material.OBSIDIAN);
		recipe2.setIngredient('Z', Material.ZOMBIE_HEAD);
		recipe2.setIngredient('N', Material.NETHER_STAR);
		Bukkit.addRecipe(recipe2);
		
		ItemStack spawner2 = api.getSpawnerFromEntityType(EntityType.SKELETON);
        NamespacedKey key3 = new NamespacedKey(this, "skeleton_spawner");
		ShapedRecipe recipe3 = new ShapedRecipe(key3, spawner2);
		recipe3.shape("DND", "DZD", "DDD");
		recipe3.setIngredient('D', Material.OBSIDIAN);
		recipe3.setIngredient('Z', Material.SKELETON_SKULL);
		recipe3.setIngredient('N', Material.NETHER_STAR);
		Bukkit.addRecipe(recipe3);
		
		for(World w : Bukkit.getWorlds()) {
			for(Entity e : w.getEntities())
			{
				if (e instanceof Damageable) {
					String name = e.getName();
					if (name != null) {
						name = ChatColor.stripColor(name);
						String[] lv = name.split("Lv. ");
						if (lv.length > 1) {
							int level = Integer.parseInt(lv[1].split("]")[0]);
							
							if (level == 5) {
								lvl5mobs.add(e);
							} else if (level == 4) {
								lvl4mobs.add(e);
							}
						}
					}
				}
			}
		}
		
		this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				try {
					if (lvl5mobs != null)
						for(Entity e : lvl5mobs) {
							try {
								if (e == null || e.isDead()) {
									lvl5mobs.remove(e);
								} else {
									for (int i = -3; i < 3; i++)
										for (int j = -3; j < 3; j++)
											e.getLocation().getWorld().spawnParticle(Particle.SPELL_MOB, e.getLocation().add(new Vector(i / 3, 1, j / 3)), 0, 1, 0, 0, 1);
								}
							} catch(Exception ex) {
								lvl5mobs.remove(e);
							}
						}
					if (lvl4mobs != null)
						for(Entity e : lvl4mobs) {
							try {
								if (e == null || e.isDead()) {
									lvl4mobs.remove(e);
								} else {
									for (int i = -1; i <= 1; i++)
										for (int j = -1; j <= 1; j++)
											e.getLocation().getWorld().spawnParticle(Particle.SPELL_MOB, e.getLocation().add(new Vector(i / 3, 1, j / 3)), 0, 1, 1, 0, 1);
								}
							} catch(Exception ex) {
								lvl4mobs.remove(e);
							}
						}
				} catch(Exception ex2) {
				}
			}
			
		}, 0, 10);
		
		System.out.println("[" + getName() + "] Enabled!");
	}
	
	public void saveTotems() {
		int index = 0;
		totemConfig.set("totems", null);
		if (totems.size() > 0) {
			for(Totem t : totems) {
				totemConfig.set("totems." + index + ".world", t.getLocation().getWorld().getName());
				totemConfig.set("totems." + index + ".x", t.getLocation().getX());
				totemConfig.set("totems." + index + ".y", t.getLocation().getY());
				totemConfig.set("totems." + index + ".z", t.getLocation().getZ());
				totemConfig.set("totems." + index + ".weight", t.getWeight());
				index++;
			}
		}
		config.saveConfig("totems.yml");
	}
	
	public void onDisable() {
		NamespacedKey key = new NamespacedKey(this, "hp_apple");
		Bukkit.removeRecipe(key);
		NamespacedKey key2 = new NamespacedKey(this, "zombie_spawner");
		Bukkit.removeRecipe(key2);
		NamespacedKey key3 = new NamespacedKey(this, "skeleton_spawner");
		Bukkit.removeRecipe(key3);
		System.out.println("[" + getName() + "] Disabled!");
	}
	
	public int getRandomNumber(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}
	
	public ItemStack getRandomEnchantBook() {
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		
		EnchantmentStorageMeta AMeta = (EnchantmentStorageMeta) item.getItemMeta();
		
		Enchantment[] enchants = {
			Enchantment.ARROW_DAMAGE,
			Enchantment.ARROW_FIRE,
			Enchantment.ARROW_INFINITE,
			Enchantment.ARROW_KNOCKBACK,
			Enchantment.CHANNELING,
			Enchantment.DAMAGE_ALL,
			Enchantment.DAMAGE_ARTHROPODS,
			Enchantment.DAMAGE_UNDEAD,
			Enchantment.DEPTH_STRIDER,
			Enchantment.DIG_SPEED,
			Enchantment.DURABILITY,
			Enchantment.FIRE_ASPECT,
			Enchantment.FROST_WALKER,
			Enchantment.IMPALING,
			Enchantment.KNOCKBACK,
			Enchantment.LOYALTY,
			Enchantment.LUCK,
			Enchantment.MENDING,
			Enchantment.MULTISHOT,
			Enchantment.OXYGEN,
			Enchantment.PIERCING,
			Enchantment.PROTECTION_ENVIRONMENTAL,
			Enchantment.PROTECTION_EXPLOSIONS,
			Enchantment.PROTECTION_FALL,
			Enchantment.PROTECTION_FIRE,
			Enchantment.PROTECTION_PROJECTILE,
			Enchantment.QUICK_CHARGE,
			Enchantment.RIPTIDE,
			Enchantment.SILK_TOUCH,
			Enchantment.SWEEPING_EDGE,
			Enchantment.THORNS,
			Enchantment.WATER_WORKER
		};
		for (int i = 0; i < rand.nextInt(2) + 1; i++) {
			Enchantment added = enchants[rand.nextInt(enchants.length)];
			int level = getRandomNumber(added.getStartLevel(), added.getMaxLevel());
			//System.out.println("Add: " + added.getName() + " from: " + added.getStartLevel() + " | to: " + added.getMaxLevel() + " | get: " + level);
			AMeta.addStoredEnchant(added, level, true);
		}
		item.setItemMeta(AMeta);
		EnchantManager.populateEnchantments(item, ObtainType.LOOT_GENERATION);
		return item;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		final Block block = event.getBlock();
		ItemStack stack = event.getItemInHand();
		if (stack == null)
			return;
		ItemMeta meta = stack.getItemMeta();
		if (meta == null)
			return;
		if(meta.getDisplayName() != null && meta.getDisplayName().equals(lucky.getItemMeta().getDisplayName())) {
			event.setCancelled(true);
			if (stack.getAmount() > 1) {
				stack.setAmount(stack.getAmount() - 1);
				event.getPlayer().getInventory().setItemInMainHand(stack);
			} else {
				event.getPlayer().getInventory().removeItem(stack);
			}
			event.getPlayer().updateInventory();
			
			switch(rand.nextInt(20)) {
				case 0:
					for(int i = 0; i < 3; i++)
						block.getLocation().getWorld().spawnEntity(block.getLocation(), EntityType.CREEPER);
					break;
				case 1:
					for(int i = 0; i < 3; i++)
						block.getLocation().getWorld().spawnEntity(block.getLocation(), EntityType.ZOMBIE);
					break;
				case 2:
					for (int i = -2; i <= 2; i++)
						for (int j = -2; j <= 2; j++) {
							Block binl = block.getLocation().getWorld().getBlockAt(block.getLocation().add(new Vector(i, 0, j)));
							if (binl.getType().equals(Material.AIR))
								binl.setType(Material.LAVA);
						}
					break;
				case 3:
					block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.DIAMOND_BLOCK, 1));
					break;
				case 4:
					block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.EMERALD_BLOCK, 1));
					break;
				case 5:
					event.getPlayer().teleport(event.getPlayer().getLocation().add(new Vector(0, 250, 0)));
					break;
				case 6:
					event.getPlayer().getLocation().getWorld().spawnFallingBlock(event.getPlayer().getLocation().add(new Vector(0, 10, 0)), Material.ANVIL, (byte) 0);
					break;
				case 7:
					for (int i = -2; i <= 2; i++)
						for (int j = -2; j <= 2; j++) {
							Block binl = block.getLocation().getWorld().getBlockAt(block.getLocation().add(new Vector(i, 0, j)));
							if (binl.getType().equals(Material.AIR))
								binl.setType(Material.WATER);
						}
					break;
					
				case 8:
					for (int z = 0; z < 3; z++)
						for (int i = -2; i < 2; i++)
							for (int j = -2; j < 2; j++) {
								if (i == 0 && j == 0)
									continue;
								Block binl = event.getPlayer().getLocation().getWorld().getBlockAt(event.getPlayer().getLocation().add(new Vector(i, z, j)));
								if (binl.getType().equals(Material.AIR))
									switch(rand.nextInt(7)) {
									case 0:
										binl.setType(Material.BLACK_WOOL);
										break;
									case 1:
										binl.setType(Material.RED_WOOL);
										break;
									case 2:
										binl.setType(Material.BLUE_WOOL);
										break;
									case 3:
										binl.setType(Material.CYAN_WOOL);
										break;
									case 4:
										binl.setType(Material.LIME_WOOL);
										break;
									case 5:
										binl.setType(Material.GREEN_WOOL);
										break;
									case 6:
										binl.setType(Material.ORANGE_WOOL);
										break;
									case 7:
										binl.setType(Material.MAGENTA_WOOL);
										break;
									}
									
							}
					break;
					
				case 9:
					for (int z = 0; z < 3; z++)
						for (int i = -2; i <= 2; i++)
							for (int j = -2; j <= 2; j++) {
								if (i == 0 && j == 0)
									continue;
								Block binl = event.getPlayer().getLocation().getWorld().getBlockAt(event.getPlayer().getLocation().add(new Vector(i, z, j)));
								if (binl.getType().equals(Material.AIR) || binl.getType().equals(Material.WATER))
									binl.setType(Material.GLASS);
									
								event.getPlayer().getLocation().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.ZOMBIE);
							}
					break;
				case 10:
					for(int i = 0; i < 8; i++)
						block.getLocation().getWorld().spawnEntity(block.getLocation(), EntityType.SILVERFISH);
					break;
				case 11:
					block.setType(Material.CAKE);
					break;
				case 12:
					for(int i = -1; i < 1; i++)
						for(int j = -1; j < 1; j++)
						block.getWorld().dropItemNaturally(block.getLocation().add(new Vector(i, 10, j)), new ItemStack(Material.GOLDEN_APPLE, 1));
					break;
				case 13:
					for(int i = -1; i < 1; i++)
						for(int j = -1; j < 1; j++)
						block.getWorld().dropItemNaturally(block.getLocation().add(new Vector(i, 10, j)), new ItemStack(Material.COOKIE, 1));
					break;
				case 14:
					/*
					for (ItemStack itemStack : event.getPlayer().getInventory().getContents()) {
						if (itemStack != null && !itemStack.getType().equals(Material.AIR)) {
							event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), itemStack);
							event.getPlayer().getInventory().removeItem(itemStack);
						}
                    }
					event.getPlayer().updateInventory();
					*/
					event.getPlayer().getLocation().getWorld().spawnParticle(Particle.HEART, event.getPlayer().getLocation(), 20);
					event.getPlayer().sendMessage("�4Szeretet!");
					break;
				case 15:
					for (ItemStack itemStack : event.getPlayer().getInventory().getArmorContents()) {
						if (itemStack != null && !itemStack.getType().equals(Material.AIR)) {
							event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), itemStack);
							event.getPlayer().getInventory().removeItem(itemStack);
						}
					}
					event.getPlayer().getInventory().setHelmet(null);
					event.getPlayer().getInventory().setBoots(null);
					event.getPlayer().getInventory().setChestplate(null);
					event.getPlayer().getInventory().setLeggings(null);
					event.getPlayer().updateInventory();
					break;
				case 16:
					event.getPlayer().setHealth(1);
					break;
				case 17:
					event.getPlayer().setLevel(0);
					event.getPlayer().setExp(0);
					event.getPlayer().sendMessage("rip level :(");
					break;
				case 18:
					event.getPlayer().setAbsorptionAmount(0);
					break;
				case 19:
					event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 20000, 4));
					break;
				case 20:
					event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60 * 5 * 20, 4));
					break;
			}
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerCostumeEvent(PlayerItemConsumeEvent event) {
		ItemMeta meta = event.getItem().getItemMeta();
		if (meta != null && meta.getDisplayName().equals(hpApple.getItemMeta().getDisplayName()) && meta.getLore().equals(hpApple.getItemMeta().getLore())) {
			if (event.getPlayer().getMaxHealth() >= 40) {
				event.setCancelled(true);
				return;
			}
			event.getPlayer().setMaxHealth(event.getPlayer().getMaxHealth() + 2);
		}
	}
	
	 public static double getDamageReduced(Player player) {
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack boots = inv.getBoots();
        ItemStack pants = inv.getLeggings();
        double red = 0.0;
        //
        if(helmet != null) {
            if (helmet.getType() == Material.LEATHER_HELMET) red = red + 0.04;
            else if (helmet.getType() == Material.GOLDEN_HELMET) red = red + 0.06;
            else if (helmet.getType() == Material.CHAINMAIL_HELMET) red = red + 0.06;
            else if (helmet.getType() == Material.IRON_HELMET) red = red + 0.08;
            else if (helmet.getType() == Material.DIAMOND_HELMET) red = red + 0.10;
            else if (helmet.getType() == Material.NETHERITE_HELMET) red = red + 0.12;
        }
        //
        if(chest != null) {
            if (chest.getType() == Material.LEATHER_CHESTPLATE) red = red + 0.12;
            else if (chest.getType() == Material.GOLDEN_CHESTPLATE)red = red + 0.16;
            else if (chest.getType() == Material.CHAINMAIL_CHESTPLATE) red = red + 0.16;
            else if (chest.getType() == Material.IRON_CHESTPLATE) red = red + 0.20;
            else if (chest.getType() == Material.DIAMOND_CHESTPLATE) red = red + 0.26;
            else if (chest.getType() == Material.NETHERITE_CHESTPLATE) red = red + 0.32;
        }
        //
        if(pants != null) {
            if (pants.getType() == Material.LEATHER_LEGGINGS) red = red + 0.08;
            else if (pants.getType() == Material.GOLDEN_LEGGINGS) red = red + 0.1;
            else if (pants.getType() == Material.CHAINMAIL_LEGGINGS) red = red + 0.14;
            else if (pants.getType() == Material.IRON_LEGGINGS) red = red + 0.16;
            else if (pants.getType() == Material.DIAMOND_LEGGINGS) red = red + 0.20;
            else if (pants.getType() == Material.NETHERITE_LEGGINGS) red = red + 0.24;
        }
        //
        if(boots != null) {
            if (boots.getType() == Material.LEATHER_BOOTS) red = red + 0.04;
            else if (boots.getType() == Material.GOLDEN_BOOTS) red = red + 0.04;
            else if (boots.getType() == Material.CHAINMAIL_BOOTS) red = red + 0.04;
            else if (boots.getType() == Material.IRON_BOOTS) red = red + 0.08;
            else if (boots.getType() == Material.DIAMOND_BOOTS) red = red + 0.10;
            else if (boots.getType() == Material.NETHERITE_BOOTS) red = red + 0.12;
        }
        //
        return red;
    }
	
	@EventHandler
    void onPlayerHoldDiamondBoots(PlayerItemHeldEvent event)
    {
		Player p = event.getPlayer();
		boolean hasrem = false;
		ItemStack item = p.getInventory().getItem(event.getNewSlot());
		if (item != null && !item.getType().equals(Material.AIR)) {
			if(ItemLevel.isPickaxe(item) && ItemLevel.getItemLevel(item) >= 4) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 99999999, ItemLevel.getItemLevel(item) / 4, false, false));
				hasrem = true;
			}
		}
		if (!hasrem)
			p.removePotionEffect(PotionEffectType.FAST_DIGGING);
    }
	
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        if(event.getSlotType() == InventoryType.SlotType.RESULT){
            ItemStack is = event.getCurrentItem();
            if (is != null && ItemLevel.isWeapon(is) && ItemLevel.getItemLevel(is) == 0) {
    			ItemLevel.setItemLevel(is, 1);
    			ItemLevel.setItemExp(is, 0);
    		}
        }
    }
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamageEvent(EntityDamageByEntityEvent event)  {
		Entity damager = event.getDamager();
		
		if (!(damager instanceof Player) && !(damager instanceof Arrow)) {
			
			if (damager.getWorld().getName().equals("oasis")) {
				event.setDamage(event.getDamage() * 6);
			} else {
				event.setDamage(event.getDamage() * 3);
			}
		}
		
		if (damager instanceof Arrow) {
			Arrow a = (Arrow) event.getDamager();
            if (!(a.getShooter() instanceof Player)) {
            	if (damager.getWorld().getName().equals("oasis")) {
    				event.setDamage(event.getDamage() * 6);
    			} else {
    				event.setDamage(event.getDamage() * 3);
    			}
            }
		}
		
		
		if (damager instanceof Player) {
			Player dm = (Player) damager;
			 if (dm.getInventory().getItemInMainHand() != null && !dm.getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
            	event.setDamage(event.getDamage() /*- ItemLevel.getDamage(dm.getInventory().getItemInMainHand())*/ + ItemLevel.calcDamage(dm.getInventory().getItemInMainHand()));
            	
            	dm.sendMessage("Damage: " + event.getDamage());
            }
		}
		
		if (event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            if (p.isBlocking()) {
                event.setCancelled(true);
                double dam = event.getDamage() * 0.2;
                if (dam > 0)
                	p.damage(dam);
            }
           
            int protect = 0;
            int blastProtect = 0;
            for(ItemStack item : p.getInventory().getArmorContents())
            {
            	if (item != null) {
            		protect += item.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
            		blastProtect += item.getEnchantmentLevel(Enchantment.PROTECTION_EXPLOSIONS);
            	}
            }
            
            int levels = 0;
            for (ItemStack item : p.getInventory().getArmorContents()) {
                try {
	            	if (item != null && !item.getType().equals(Material.AIR)) {
		            	if (damager instanceof Monster || damager instanceof Arrow)
		            		ItemLevel.giveExp(item, (int)event.getDamage() * 2);
		            	levels += ItemLevel.getItemLevel(item);
	            	}
                }catch (Exception ex) {
                	
                }
            }
            
            double red = getDamageReduced(p);
            double def = 1.0 - ((protect / 12.0) * red * 0.5);
            def -= (double)levels / 80.0 * 0.4;
            def *= 1 - (red * 0.4);
            
            double blastDef = 1.0;
            
            if (damager instanceof TNTPrimed || damager instanceof Creeper) {
            	blastDef = 1 - ((blastProtect / 16.0) * 0.5);
            }
            
            double origDamage = event.getDamage();
            event.setDamage(origDamage * def * blastDef);
            
            //p.sendMessage("orig damage: " + origDamage + " | dam: " + event.getDamage() + " | prot: " + protect + " | def: " + def + " | blastDef: " + blastDef + " | armor: " + levels);
        }
	}
	
	public boolean isTotemDeff(Block block) {
		for (Totem t : totems) {
    		if (block.getLocation().distance(t.getLocation()) < t.getWeight() * 5) {
    			return true;
    		}
    	}
		return false;
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
	    if (event.getEntity() instanceof Creeper) {
	    	event.blockList().removeIf(block -> isTotemDeff(block));
	    	event.blockList().removeIf(block -> block.getType() == Material.CHEST);
	    	
	    	String name = event.getEntity().getName();
			if (name != null) {
				name = ChatColor.stripColor(name);
				String[] lv = name.split("Lv. ");
				if (lv.length > 1) {
					int level = Integer.parseInt(lv[1].split("]")[0]);
					
					event.setYield(event.getYield() * level);
				}
			}
	    }
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageEvent e) {
        EntityDamageEvent.DamageCause cause = e.getCause();
        EntityType entity = e.getEntity().getType();
        if (entity.equals(EntityType.DROPPED_ITEM) && (cause.equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) || cause.equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) || cause.equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK))) {
        	e.setCancelled(true);
        }
        
       if (cause == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {
    	   String name = e.getEntity().getName();
			if (name != null) {
				name = ChatColor.stripColor(name);
				String[] lv = name.split("Lv. ");
				if (lv.length > 1) {
					int level = Integer.parseInt(lv[1].split("]")[0]);
		
					e.setDamage(10 + level * 4);
				}
			}
       } else if (cause == EntityDamageEvent.DamageCause.HOT_FLOOR) {
    	   e.setDamage(e.getDamage() * 8);
       }
       
       damageevent = e.isCancelled();
    }
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event) {
		try {
			ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
			if (item != null && !item.getType().equals(Material.AIR)) {
				if (ItemLevel.isPickaxe(item)) {
					ItemLevel.giveExp(item, ItemLevel.getMiningExp(event.getBlock().getType()));
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		if (event.getBlock().getType().equals(Material.NETHER_QUARTZ_ORE))
			event.setExpToDrop(event.getExpToDrop() * 2);
		else
			event.setExpToDrop(event.getExpToDrop() * 3);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player p = event.getPlayer();
		if (p.getBedSpawnLocation() != null)
			event.setRespawnLocation(p.getBedSpawnLocation());
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityDeadEvent(EntityDeathEvent event) {
		LivingEntity entity = event.getEntity();
		
		if (lvl5mobs.contains(entity))
			lvl5mobs.remove(entity);
		if (lvl4mobs.contains(entity))
			lvl4mobs.remove(entity);
		if (entity instanceof Player) {
			if (event.getEntity().getKiller() instanceof Damageable) {
				
				String name = event.getEntity().getKiller().getName();
				if (name != null) {
					name = ChatColor.stripColor(name);
					String[] lv = name.split("Lv. ");
					if (lv.length > 1) {
						int level = Integer.parseInt(lv[1].split("]")[0]);
						
						if (level > 1) {
							event.getEntity().getKiller().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 5, level));
						}
					}
				}
			}
		}
		
		if (entity != null && entity instanceof Damageable && !(entity instanceof Player)) {
			String name = entity.getName();
			if (name != null) {
				name = ChatColor.stripColor(name);
				String[] lv = name.split("Lv. ");
				if (lv.length > 1) {
					int level = Integer.parseInt(lv[1].split("]")[0]);
					
					//System.out.println(name + " dead -> level " + level);
					if (level > 0) {
						event.setDroppedExp((int)(event.getDroppedExp() * (level * 2) * 1.5));
						
						if (entity.getKiller() instanceof Player) {
							Player pl = (Player) entity.getKiller();
							ItemStack item = pl.getInventory().getItemInMainHand();
							if (item != null && !item.getType().equals(Material.AIR)) {
								if (ItemLevel.isWeapon(item))
									ItemLevel.giveExp(item, event.getDroppedExp());
							}
						}
						
						if (level >= 3 && entity.getWorld().getName().equals("oasis")) {
							event.getDrops().clear();
							if (rand.nextInt(100) < 10) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), getRandomEnchantBook());
							}
							
							return;
						}
						
						if (level >= 4) {
							if (entity instanceof Monster) {
								
								int drop = rand.nextInt(100);
								System.out.println(name + " drop item rate " + drop);
								if (drop >= 98) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.DIAMOND_BLOCK, 1));
									//return;
								}
								else if (drop > 90) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.DIAMOND, rand.nextInt(2) + 1));
									//return;
								}
								else if (drop > 80) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.EMERALD, rand.nextInt(2) + 1));
									//return;
								}
								else if (drop > 70) {
									int tool = rand.nextInt(9);
									ItemStack st = null;
									if (tool == 0)
										st = new ItemStack(Material.DIAMOND_SWORD);
									else if (tool == 1)
										st = new ItemStack(Material.DIAMOND_AXE);
									else if (tool == 2)
										st = new ItemStack(Material.DIAMOND_CHESTPLATE);
									else if (tool == 3)
										st = new ItemStack(Material.DIAMOND_PICKAXE);
									else if (tool == 4)
										st = new ItemStack(Material.DIAMOND_SHOVEL);
									else if (tool == 5)
										st = new ItemStack(Material.DIAMOND_HELMET);
									else if (tool == 6)
										st = new ItemStack(Material.DIAMOND_HOE);
									else if (tool == 7)
										st = new ItemStack(Material.DIAMOND_BOOTS);
									else if (tool == 8)
										st = new ItemStack(Material.DIAMOND_HELMET);
									else if (tool == 9)
										st = new ItemStack(Material.DIAMOND_LEGGINGS);
									st.setDurability((short) ((float)st.getDurability() * rand.nextDouble()));
									if (ItemLevel.isWeapon(st)) {
										ItemLevel.setItemLevel(st, 1 + rand.nextInt(3));
										ItemLevel.setItemExp(st, 0);
									}
									entity.getWorld().dropItemNaturally(entity.getLocation(), st);
								}
								else if (drop > 60) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), lucky);
								}
								
								if (level == 5) {
									if (entity.getKiller() instanceof Player) {
										Bukkit.broadcastMessage(entity.getKiller().getName() + " er�s volt, �s meg�lte " + entity.getName() + "-t.");
									}
								}
							}
							
							if (entity instanceof Zombie) {
								if (rand.nextInt(100) < 5) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.ZOMBIE_HEAD, 1));
								}
							} else if (entity instanceof Skeleton) {
								if (rand.nextInt(100) < 5) {
									entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.SKELETON_SKULL, 1));
								}
							}
						}
						
						if (level == 3) {
							int drop = rand.nextInt(100);
							if (drop > 95) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.EMERALD, rand.nextInt(2) + 1));
								return;
							}else if (drop > 80) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.IRON_INGOT, rand.nextInt(2) + 1));
								return;
							}else if (drop > 70) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.GOLD_INGOT, rand.nextInt(2) + 1));
								return;
							}else if (drop > 60) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.LAPIS_LAZULI, rand.nextInt(6) + 1));
								return;
							}
						}
						if (level == 2) {
							int drop = rand.nextInt(100);
							if (drop > 90) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.IRON_INGOT, rand.nextInt(2) + 1));
								return;
							}else if (drop > 80) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.COAL, rand.nextInt(2) + 1));
								return;
							} else if (drop > 70) {
								entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.REDSTONE, rand.nextInt(4) + 1));
								return;
							}
						}
					}
				}
			}
		}
	}
	
	public boolean isUnderBlock(Entity p){
		for(int y = (int)p.getLocation().getY() + 1; y <= 255; y++){
			Location l = new Location (p.getWorld(), p.getLocation().getX(), (double)y, p.getLocation().getZ());
			boolean ok = false;
			if (l.getBlock().getType() == Material.AIR)
				ok = true;
			if (l.getBlock().getType().name().contains("LEAVES"))
				ok = true;
			if (l.getBlock().getType().name().contains("WATER"))
				ok = true;
			if (l.getBlock().getType().name().contains("LAVA"))
				ok = true;
			if (!ok)
				return true;
		}
		return false;
	}
	
	public static ItemStack getHead(String url) {
		@SuppressWarnings("deprecation")
		ItemStack head = new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3);
		if (url.isEmpty()) {
			return head;
		}
		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		if (url.startsWith("http://")) {
			byte[] encodedData = Base64
					.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", new Object[] { url }).getBytes());
			profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
		} else {
			profile.getProperties().put("textures", new Property("textures", url));
		}
		Field profileField = null;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			System.out.println("Failed set head texture!");
		}
		head.setItemMeta(headMeta);
		return head;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntitySpawnEvent(EntitySpawnEvent event) {
		Entity entity = event.getEntity();
		if (entity != null) {
			if (entity instanceof ItemFrame || entity instanceof ArmorStand)
				return;
			
			String name = entity.getName();
			if (name != null) {
				name = ChatColor.stripColor(name);
				String[] lv = name.split("Lv. ");
				if (lv.length > 1) {
					return;
				}
			}
			
			if (!(entity instanceof Player) && !(entity instanceof Item) && !(entity instanceof Arrow) && entity instanceof Damageable && !(entity instanceof Bee)) {
				int i = rand.nextInt(100);
				
				if (isUnderBlock(entity) && entity instanceof Monster)
					i += rand.nextInt(30);
				
				if (entity.getLocation().getY() < 40)
					i += rand.nextInt(20);
				
				if (getLightLevel(entity.getLocation().getBlock()) == 0 && isUnderBlock(entity))
					i += rand.nextInt(50);
				
				if (entity.getWorld().getName().equals("oasis"))
					i += 60;
				
				int level = 1;
				String color = "";
				EntityInsentient nmsEntity = (EntityInsentient) ((CraftLivingEntity) entity).getHandle();

				if (entity instanceof Zombie)
					if(((Zombie) entity).isBaby())
						name = "Kis Vendel";
				
				if (!(entity instanceof Monster))
					i -= rand.nextInt(50);
				
				if (i >= 120) {
					level = 5;
					color = "�e";
					AttributeModifiable ins10 = nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
					if (ins10 != null)
						if (entity.getWorld().getName().equals("oasis")) {
							ins10.setValue(ins10.getBaseValue() * 1.95);
						} else {
							ins10.setValue(ins10.getBaseValue() * 1.75);
						}
					AttributeModifiable ins11 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_SPEED);
					if (ins11 != null)
						if (entity.getWorld().getName().equals("oasis")) {
							ins11.setValue(ins11.getBaseValue() * 4);
						} else {
							ins11.setValue(ins11.getBaseValue() * 3);
						}
					AttributeModifiable ins12 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE);
					if (ins12 != null)
						ins12.setValue(ins12.getBaseValue() * 3);
					AttributeModifiable ins13 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_KNOCKBACK);
					if (ins13 != null)
						ins13.setValue(ins13.getBaseValue() * 2);
					nmsEntity.setAggressive(true);
					
					AttributeModifiable ins14 = nmsEntity.getAttributeInstance(GenericAttributes.MAX_HEALTH);
					if (ins14 != null)
						ins14.setValue(ins14.getBaseValue() * 2);
					
					/*
					if (entity instanceof Monster) {
						Player nearest = null;
						double mindist = 30;
						for(Player p : Bukkit.getOnlinePlayers()) {
							if (p.getWorld().getName().equals(entity.getWorld().getName())) {
								double dist = p.getLocation().distance(entity.getLocation());
								if (dist < mindist) {
									mindist = dist;
									nearest = p;
								}
							}
						}
						if (nearest != null)
							Bukkit.broadcastMessage("�f[�yLv. " + level + "�f] " + name + " j�tt j�tszani " + nearest.getName() + "-val/vel (" + Math.ceil(mindist) + "m).");
					}*/
					lvl5mobs.add(entity);
					
				}else if (i > 90) {
					level = 4;
					color = "�4";
					AttributeModifiable ins2 = nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
					if (entity.getWorld().getName().equals("oasis")) {
						ins2.setValue(ins2.getBaseValue() * 1.45);
					} else {
						ins2.setValue(ins2.getBaseValue() * 1.25);
					}
					AttributeModifiable ins3 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_SPEED);
					if (ins3 != null)
						ins3.setValue(ins3.getBaseValue() * 2);
					AttributeModifiable ins5 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE);
					if (ins5 != null)
						ins5.setValue(ins5.getBaseValue() * 2);
					lvl4mobs.add(entity);
				}
				else if (i > 75) {
					level = 3;
					color = "�c";
					AttributeModifiable ins2 = nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
					if (ins2 != null)
						ins2.setValue(ins2.getBaseValue() * 1.25);
					AttributeModifiable ins3 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_SPEED);
					if (ins3 != null)
						ins3.setValue(ins3.getBaseValue() * 2);
		
				} else if (i > 60) {
					level = 2;
					color = "�6";
					AttributeModifiable ins4 = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_SPEED);
					if (ins4 != null)
						ins4.setValue(ins4.getBaseValue() * 2);
				}
				
				entity.setCustomName("�f[" + color + "Lv. " + level + "�f] �f" + name);

				AttributeModifiable ins = nmsEntity.getAttributeInstance(GenericAttributes.MAX_HEALTH);
				if (ins != null) {
					if (entity.getWorld().getName().equals("oasis")) {
						ins.setValue(ins.getBaseValue() * level * 2);
					} else {
						ins.setValue(ins.getBaseValue() * level);
					}
				}
				nmsEntity.setHealth((float)ins.getBaseValue());
				
				if (entity instanceof Creeper) {
					((Creeper) entity).setExplosionRadius(((Creeper) entity).getExplosionRadius() + level);
				}
			}
		}
	}
	
	  @EventHandler(priority = EventPriority.HIGH)
	  public void onPlayerInteract(PlayerInteractEvent e) {
	    Player p = e.getPlayer();
	    if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock() != null && e.getClickedBlock().getType().equals(Material.LADDER) && p.isSneaking()) {
	      Location l = e.getClickedBlock().getLocation();
	      while (true) {
	        l.setY(l.getY() + 1.0D);
	        if (!l.getBlock().getType().equals(Material.LADDER)) {
	          Location tpl = l.clone();
	          tpl.setX(tpl.getX() + 0.5D);
	          tpl.setZ(tpl.getZ() + 0.5D);
	          tpl.setPitch(p.getLocation().getPitch());
	          tpl.setYaw(p.getLocation().getYaw());
	          p.teleport(tpl);
	          break;
	        } 
	      } 
	    } 
	  }
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBedEnter(PlayerBedEnterEvent event)
	{
		if (event.isCancelled())
			return;
		
		if (event.getBedEnterResult() != BedEnterResult.OK)
			return;
		
		sleepers++;
		
		skipping = false;
		
		Player player = event.getPlayer();
		player.setStatistic(Statistic.TIME_SINCE_REST, 0);
		world = event.getBed().getLocation().getWorld();
		
		player.setBedSpawnLocation(player.getLocation(), true);
		
		float ratio = sleepers / (float)Bukkit.getOnlinePlayers().size();
		
		Bukkit.broadcastMessage("�f" + player.getName() + " lefek�dt aludni! [" + (int)Math.floor(ratio * 100) + "%]");

		if(ratio >= 0.5)
		{
			skipping = true;
			
			this.getServer().getScheduler().runTaskLater(this, new Runnable() {
				@Override
				public void run() {
					while(world.getTime() > 20)
					{
						world.setTime(world.getTime() + 10);
					}
					world.setThundering(false);
				}
			}, 20);
			
			sleepers = 0;
			
			Bukkit.broadcastMessage("�f�taludt�tok az �jszak�t...");
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBedLeave(PlayerBedLeaveEvent event)
	{
		sleepers--;
		if (sleepers < 0)
			sleepers = 0;
		Player player = event.getPlayer();
		if(skipping)
		{	
			return;
		}
		
		Bukkit.broadcastMessage("�f" + player.getName() + " elhagyta az �gyat!");
	}
	
	public int getLightLevel(Block block) {
		int light = 0;
		final BlockFace[] blockFaces = {BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};
		for (BlockFace blockFace: blockFaces) {
		    light = Math.max(light, block.getRelative(blockFace).getLightLevel());
		    if (light >= 15) {
		        break;
		    }
		}
		light = light * 100 / 15;
		return light;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains("_WALL_SIGN")) {
				Sign s = (Sign) event.getClickedBlock().getState();
				Player p = event.getPlayer();

				if (ChatColor.stripColor(s.getLine(0)).equals("[ItemToDia]")) {
					
					ItemStack item = p.getInventory().getItemInMainHand();
					if (item != null) {
						int added = 0;
						switch(item.getType()) {
						case DIAMOND_SWORD:
						case DIAMOND_SHOVEL:
						case DIAMOND_PICKAXE:
						case DIAMOND_HOE:
						case DIAMOND_AXE:
							added = 1;
							break;
						case DIAMOND_HELMET:
						case DIAMOND_BOOTS:
							added = 2;
							break;
						case DIAMOND_CHESTPLATE:
							added = 4;
							break;
						case DIAMOND_LEGGINGS:
							added = 3;
							break;
						default:
							break;
						}
						
						if (added > 0) {
							ItemStack dia = new ItemStack(Material.DIAMOND, added);
							try {
					    		p.getInventory().addItem(dia);
					    	} catch(Exception e) {
					    		p.getWorld().dropItemNaturally(p.getLocation(), dia);
					    	}
							p.getInventory().setItemInMainHand(null);
							p.updateInventory();
							p.sendMessage("�7Kapt�l �e" + added + " darab �7gy�m�ntot.");
						} else {
							p.sendMessage("�7Ez�rt a t�rgy�rt nem kapsz semmit.");
						}
					}
				}
			}
		}
	}
	
	public void printToConsole(String msg) {
	    getServer().getConsoleSender().sendMessage(msg);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	    if (cmd.getName().equals("itemlevel") || cmd.getName().equals("il")) {
		  int level = 0;
		  try {
			  level = Integer.parseInt(args[0]);
		  }catch(Exception e) {
			  
		  }
		  
		  if (sender instanceof Player) {
			  Player p = (Player) sender;
			  
			  ItemLevel.setItemLevel(p.getInventory().getItemInMainHand(), level);
		  }
	    	return true;
	    } 
	    else if (cmd.getName().equals("light")) {
	    	if (sender instanceof Player) {
				  Player p = (Player) sender;
				  
				  p.sendMessage("Abs: " + getLightLevel(p.getLocation().getBlock()));
			  }
	    	return true;
		}
	    else if (cmd.getName().equals("givelucky")) {
	    	Player p = Bukkit.getPlayer(args[0]);
	    	try {
	    		p.getInventory().addItem(lucky);
	    	} catch(Exception e) {
	    		p.getWorld().dropItemNaturally(p.getLocation(), lucky);
	    	}
	    }
	    else if (cmd.getName().equals("closeportal")) {
	    	Bukkit.broadcastMessage("A port�l nyitva van 60 m�sodpercig!");
	    	this.getServer().getScheduler().runTaskLater(this, new Runnable() {

				@Override
				public void run() {
					Bukkit.broadcastMessage("A port�l bez�rult!");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gate close oasis");
				}
	    		
	    	}, 20 * 60);
	    }
	    else if (cmd.getName().equals("randombook")) {
	    	if (sender instanceof Player) {
				  Player p = (Player) sender;
				  p.getInventory().addItem(getRandomEnchantBook());
			 }
	    	return true;
	    }
	    return false;
	  }
}
