package hu.mark.mobdifi;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.Vector;

public class TotemEvent implements Listener {
	private MobDifi plugin;
	private List<Location> totemPosition;
	private boolean hasJackO;
	private World world;
			
	public TotemEvent(MobDifi p) {
		this.plugin = p;
		this.totemPosition = new ArrayList<Location>();
		this.hasJackO = false;
	}
	
	public void findFences(Location loc) {
		Block b = loc.getBlock();
		if (b.getType().equals(Material.AIR))
			return;
		
		if (this.totemPosition.contains(loc))
			return;
		
		if (b.getType().equals(Material.OAK_FENCE) || (b.getType().equals(Material.JACK_O_LANTERN) && !this.hasJackO)) {
			this.totemPosition.add(loc.clone());
			
			if (b.getType().equals(Material.JACK_O_LANTERN))
				this.hasJackO = true;
			
			Location left = loc.clone().add(new Vector(1, 0, 0));
			Location right = loc.clone().add(new Vector(-1, 0, 0));
			Location front = loc.clone().add(new Vector(0, 0, 1));
			Location back = loc.clone().add(new Vector(0, 0, -1));
			Location top = loc.clone().add(new Vector(0, 1, 0));
			Location down = loc.clone().add(new Vector(0, -1, 0));
			findFences(left);
			findFences(right);
			findFences(back);
			findFences(front);
			findFences(top);
			findFences(down);
		}
	}
	
	public Vector getCentroid() {
	    double centroidX = 0, centroidY = 0, centroidZ = 0;
	    int i = 0;
	    for (Location loc : this.totemPosition) {
	    	if (loc.getBlock().getType().equals(Material.OAK_FENCE)) {
		        centroidX += loc.getX();
		        centroidY += loc.getY();
		        centroidZ += loc.getZ();
		        i++;
	    	}
	    }
	    return new Vector(centroidX / i, centroidY / i, 
	            centroidZ / i);
	}
	
	public Location findHighestPosition(Material mat) {
		Location hl = new Location(world, 0, Integer.MAX_VALUE * -1, 0);
		for(Location loc : this.totemPosition)
			if (loc.getY() > hl.getY() && loc.getBlock().getType().equals(mat))
				hl = loc;
		return hl;
	}
	
	public Location findLargestX(Material mat) {
		Location hl = new Location(world, Integer.MAX_VALUE * -1, 0, 0);
		for(Location loc : this.totemPosition)
			if (loc.getX() > hl.getX() && loc.getBlock().getType().equals(mat))
				hl = loc;
		return hl;
	}
	
	public Location findLargestZ(Material mat) {
		Location hl = new Location(world, 0, 0, Integer.MAX_VALUE * -1);
		for(Location loc : this.totemPosition)
			if (loc.getZ() > hl.getZ() && loc.getBlock().getType().equals(mat))
				hl = loc;
		return hl;
	}
	
	public boolean isFullTotem(Location loc) {
		this.totemPosition.clear();
		this.hasJackO = false;
		this.world = loc.getWorld();
		findFences(loc);
		
		Vector center = this.getCentroid();
		Location largestX = this.findLargestX(Material.OAK_FENCE);
		Location largestZ = this.findLargestZ(Material.OAK_FENCE);
		double weightX = largestX.getX() - center.getX();
		double weightZ = largestZ.getZ() - center.getZ();
		
		Location maxJack = this.findHighestPosition(Material.JACK_O_LANTERN);
		Location maxFence = this.findHighestPosition(Material.OAK_FENCE);
		
		return this.totemPosition.size() >= 5 && this.hasJackO && maxJack.getY() > maxFence.getY() && maxJack.getBlock().getType().equals(Material.JACK_O_LANTERN)
				&& maxJack.getX() == center.getX() && maxJack.getZ() == center.getZ() && (weightX >= 1 || weightZ >= 1);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlock().getType().equals(Material.JACK_O_LANTERN)) {
			if(isFullTotem(event.getBlock().getLocation())) {
				Vector center = this.getCentroid();
				plugin.totems.add(new Totem(new Location(world, center.getBlockX(), center.getBlockY(), center.getBlockZ()), this.totemPosition.size()));
				plugin.saveTotems();
				event.getPlayer().sendMessage("�7L�trehozt�l egy totemet!");
			}
		} else if (event.getBlock().getType().equals(Material.OAK_FENCE)) {
			this.totemPosition.clear();
			findFences(event.getBlock().getLocation());
			List<Location> clonePos = new ArrayList<Location>(this.totemPosition);
			event.getBlock().setType(Material.AIR);
			for (Location loc : clonePos) {
				if (isFullTotem(loc)) {
					event.setCancelled(true);
					event.getPlayer().sendMessage("�4El�bb a totem fej�t szedd le!");
					return;
				}
			}
			event.getBlock().setType(Material.OAK_FENCE);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if (event.getBlock().getType().equals(Material.JACK_O_LANTERN) || event.getBlock().getType().equals(Material.OAK_FENCE))
			if(isFullTotem(event.getBlock().getLocation())) {
				if (event.getBlock().getType().equals(Material.OAK_FENCE)) {
					event.getPlayer().sendMessage("�4El�bb a totem fej�t szedd le!");
					event.setCancelled(true);
				} else {
					Vector center = this.getCentroid();
					
					try {
						plugin.totems.removeIf(f -> f.loc.getWorld().getName().equals(world.getName()) &&
								f.loc.getBlockX() == center.getBlockX() &&
								f.loc.getBlockY() == center.getBlockY() &&
								f.loc.getBlockZ() == center.getBlockZ());
					} catch(Exception e) {
						System.out.println("ex2: " + e.getMessage());
					}
					
					plugin.saveTotems();
					event.getPlayer().sendMessage("�7Megsemmis�tetted a totemet!");
				}
			}
	}
}
