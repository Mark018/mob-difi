package hu.mark.mobdifi;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.dnyferguson.mineablespawners.nbtapi.NBTItem;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import net.md_5.bungee.api.ChatColor;
import su.nightexpress.goldenenchants.manager.EnchantManager;

public class ItemLevel {
	private static int MAX_LEVEL = 20;
	private static int[] EXP = {
		500, // 1
		2000, // 2
		6000, // 3
		10000, // 4
		20000, // 5
		30000, // 6
		40000, // 7
		60000, // 8
		80000, // 9
		120000, // 10
		160000, // 11
		200000, // 12
		240000, // 13
		300000, // 14
		400000, // 15
		500000, // 16
		750000, // 17
		1000000, // 18
		1500000, // 19
		2000000 // 20
	};
	public static int getItemLevel(ItemStack item) {
		NBTItem nbti = new NBTItem(item);
		if(!nbti.hasNBTData())
			return 0;
		if(nbti.hasKey("item_level"))
			return nbti.getInteger("item_level");
        return 0;
	}
	
	public static int getCurrentExp(ItemStack item) {
		NBTItem nbti = new NBTItem(item);
		if(!nbti.hasNBTData())
			return 0;
		if(nbti.hasKey("item_exp"))
			return nbti.getInteger("item_exp");
        return 0;
	}
	
	public static int getNextExp(ItemStack item) {
		int level = getItemLevel(item);
		if (level >= MAX_LEVEL)
			return 0;
		return EXP[level];
	}
	
	private static String getBar(int curr, int max) {
		float a = (float)curr / (float)max;
		int s = (int)Math.floor(a * 30);
		String str = "�f[�a";
		for (int i = 0; i < 30; i++) {
			if (s == i)
				str += "�7";
			str += "|";
		}
		str += "�f]";
		return str;
	}
	
	public static <K, V> Multimap<K, V> createMultiMap(Map<K, ? extends Iterable<V>> input) {
		  Multimap<K, V> multimap = ArrayListMultimap.create();
		  for (Map.Entry<K, ? extends Iterable<V>> entry : input.entrySet()) {
		    multimap.putAll(entry.getKey(), entry.getValue());
		  }
		  return multimap;
	}

	public static double getDamage(ItemStack item) {
		switch(item.getType()) {
		case WOODEN_SWORD:
			return 4.0;
		case STONE_SWORD:
			return 5.0;
		case GOLDEN_SWORD:
			return 4.0;
		case IRON_SWORD:
			return 6.0;
		case DIAMOND_SWORD:
			return 7.0;
		case NETHERITE_SWORD:
			return 8.0;
		case WOODEN_AXE:
			return 2.0;
		case STONE_AXE:
			return 9.0;
		case GOLDEN_AXE:
			return 7.0;
		case IRON_AXE:
			return 9.0;
		case DIAMOND_AXE:
			return 9.0;
		case NETHERITE_AXE:
			return 10.0;
		case WOODEN_PICKAXE:
			return 2.0;
		case STONE_PICKAXE:
			return 3.0;
		case GOLDEN_PICKAXE:
			return 2.0;
		case IRON_PICKAXE:
			return 4.0;
		case DIAMOND_PICKAXE:
			return 5.0;
		case NETHERITE_PICKAXE:
			return 6.0;
		default:
			break;
			
		}
		return 1;
	}
	
	public static double getSpeed(ItemStack item) {
		switch(item.getType()) {
			case WOODEN_SWORD:
			case STONE_SWORD:
			case GOLDEN_SWORD:
			case IRON_SWORD:
			case DIAMOND_SWORD:
			case NETHERITE_SWORD:
				return 1.6;
			case WOODEN_AXE:
				return 0.8;
			case STONE_AXE:
				return 0.8;
			case GOLDEN_AXE:
				return 1.0;
			case IRON_AXE:
				return 0.9;
			case DIAMOND_AXE:
				return 1.0;
			case NETHERITE_AXE:
				return 1.0;
			case WOODEN_PICKAXE:
				return 1.2;
			case STONE_PICKAXE:
				return 1.2;
			case GOLDEN_PICKAXE:
				return 1.2;
			case IRON_PICKAXE:
				return 1.2;
			case DIAMOND_PICKAXE:
				return 1.2;
			case NETHERITE_PICKAXE:
				return 1.2;
			default:
				break;
			
		}
		return 0;
	}
	
	public static double calcDamage(ItemStack item) {
		int damMultiple = item.getEnchantmentLevel(Enchantment.DAMAGE_ALL) + item.getEnchantmentLevel(Enchantment.ARROW_DAMAGE);
		return (getDamage(item) + getItemLevel(item)) * (1 + (damMultiple * 0.5));
	}
	
	public static int getMiningExp(Material mat) {
		switch(mat) {
		case OBSIDIAN:
			return 50;
		case DIAMOND_ORE:
			return 150;
		case GOLD_ORE:
			return 100;
		case EMERALD_ORE:
			return 200;
		case IRON_ORE:
			return 20;
		case COAL_ORE:
			return 5;
		case LAPIS_ORE:
			return 10;
		case REDSTONE_ORE:
			return 10;
		case NETHER_QUARTZ_ORE:
			return 20;
		default:
			return 1;
		}
	}
	
	public static void updateItemMeta(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		int curr = getCurrentExp(item);
		int next = getNextExp(item);
		
		EnchantManager.updateItemLoreEnchants(item);
		
		if (meta.getLore() != null && meta.getLore().size() > 0)
		{
			List<String> lore = meta.getLore();
			
			lore.removeIf(f -> f.contains("Item level") || f.contains("EXP") || ChatColor.stripColor(f).contains("[||||") || f.contains("Damage"));
			
			lore.add("�7Item level: �y" + (getItemLevel(item)));
			lore.add("�7EXP: �f" + curr + "�f/" + next);
			lore.add(getBar(curr, next));
			
			if (isWeapon(item)) {
				lore.add("�7Damage: �e" + calcDamage(item));
			}
			
			meta.setLore(lore);
		} else {
			if (isWeapon(item))
				meta.setLore(Arrays.asList(new String[] { "�7Item level: �y" + (getItemLevel(item)), "�7EXP: �f" + curr + "�f/" + next, getBar(curr, next), "�7Damage: �e" + calcDamage(item) }));
			else
				meta.setLore(Arrays.asList(new String[] { "�7Item level: �y" + (getItemLevel(item)), "�7EXP: �f" + curr + "�f/" + next, getBar(curr, next) }));
		}
		
		if (isWeapon(item)) {
			if (meta.hasAttributeModifiers()) {
				Multimap<Attribute, AttributeModifier> modi = meta.getAttributeModifiers();
				for(Entry<Attribute, AttributeModifier> e : modi.entries())
					if (e.getValue().getName().equals("generic.attackDamage") || e.getValue().getName().equals("generic.attackSpeed"))
						meta.removeAttributeModifier(e.getKey(), e.getValue());
			}
			meta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", getItemLevel(item), AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
			meta.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, new AttributeModifier(UUID.randomUUID(), "generic.attackSpeed", getSpeed(item) + (getItemLevel(item) / 20), AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
		
		} else if (isPickaxe(item)) {
			if (meta.hasAttributeModifiers()) {
				Multimap<Attribute, AttributeModifier> modi = meta.getAttributeModifiers();
				for(Entry<Attribute, AttributeModifier> e : modi.entries())
					if (e.getValue().getName().equals("generic.attackDamage") || e.getValue().getName().equals("generic.attackSpeed"))
						meta.removeAttributeModifier(e.getKey(), e.getValue());
			}
			meta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, new AttributeModifier(UUID.randomUUID(), "generic.attackDamage", getDamage(item) + (getItemLevel(item) / 10), AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
			meta.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, new AttributeModifier(UUID.randomUUID(), "generic.attackSpeed", getSpeed(item) + (getItemLevel(item) / 20), AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
		}
		
		item.setItemMeta(meta);
	}
	
	public static void setItemLevel(ItemStack item, int level) {
		NBTItem nbti = new NBTItem(item);
		nbti.setInteger("item_level", level);
		nbti.applyNBT(item);
		
		updateItemMeta(item);
	}
	
	public static void setItemExp(ItemStack item, int exp) {
		NBTItem nbti = new NBTItem(item);
		nbti.setInteger("item_exp", exp);
		nbti.applyNBT(item);
		updateItemMeta(item);
	}
	
	public static void giveExp(ItemStack item, int exp) {
		if (item == null || item.getType().equals(Material.AIR))
			return;
		int curr = getCurrentExp(item);
		
		while(exp > 0) {
			int next = getNextExp(item);
			if (next == 0)
				break;
			if (curr + exp > next) {
				exp = curr + exp - next;
				curr = 0;
				setItemExp(item, 0);
				setItemLevel(item, getItemLevel(item) + 1);
			} else {
				setItemExp(item, curr + exp);
				exp = 0;
			}
		}
	}
	
	public static boolean isWeapon(ItemStack item) {
		if (item == null || item.getType().equals(Material.AIR))
			return false;
		switch(item.getType()) {
			case WOODEN_SWORD:
			case STONE_SWORD:
			case GOLDEN_SWORD:
			case IRON_SWORD:
			case DIAMOND_SWORD:
			case NETHERITE_SWORD:
			case WOODEN_AXE:
			case STONE_AXE:
			case GOLDEN_AXE:
			case IRON_AXE:
			case DIAMOND_AXE:
			case NETHERITE_AXE:
			case BOW:
				return true;
		default:
			break;
		}
		
		return false;
	}
	
	public static boolean isPickaxe(ItemStack item) {
		if (item == null || item.getType().equals(Material.AIR))
			return false;
		if (item.getType().name().contains("_PICKAXE"))
			return true;
		
		switch(item.getType()) {
			case WOODEN_PICKAXE:
			case STONE_PICKAXE:
			case GOLDEN_PICKAXE:
			case IRON_PICKAXE:
			case DIAMOND_PICKAXE:
			case NETHERITE_PICKAXE:
				return true;
		default:
			break;
		}
		
		return false;
	} 
}
