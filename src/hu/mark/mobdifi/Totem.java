package hu.mark.mobdifi;

import org.bukkit.Location;

public class Totem {
	public Location loc;
	public int weight;
	
	public Totem(Location loc, int weight) {
		this.loc = loc;
		this.weight = weight;
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	public Location getLocation() {
		return this.loc;
	}
}
