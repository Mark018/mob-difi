package hu.mark.mobdifi;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {
	public static HashMap<String, FileConfiguration> files = new HashMap<String, FileConfiguration>();
	private static String path = "";

	public ConfigManager(String p){
		path = p;
	}
	
	public static void setPath(String p) {
		path = p;
	}

	public FileConfiguration getConfig(String name) {
		if (files.containsKey(name))
			return files.get(name);
		FileConfiguration conf_file = YamlConfiguration.loadConfiguration(getConfigFile(name));
		files.put(name, conf_file);
		return conf_file;
	}

	public File getConfigFile(String name) {
		return new File(path + name);
	}

	public boolean saveConfig(String name) {
		if (files.containsKey(name)) {
			try {
				files.get(name).save(getConfigFile(name));
				return true;
	        } catch(IOException e) {
	        	return false;
	        }
		} else {
			return false;
		}
	}
}
