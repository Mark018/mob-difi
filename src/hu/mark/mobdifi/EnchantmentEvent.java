package hu.mark.mobdifi;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import su.nightexpress.goldenenchants.manager.EnchantManager;
import su.nightexpress.goldenenchants.manager.enchants.GoldenEnchant;

public class EnchantmentEvent implements Listener {
	private MobDifi plugin;
	private boolean debug;
	
	public EnchantmentEvent(MobDifi p) {
		this.plugin = p;
		this.debug = false;
	}
	
	@EventHandler
	public void onPrepareAnvilEvent(PrepareAnvilEvent event) {
		AnvilInventory inventory = event.getInventory();
		inventory.setMaximumRepairCost(2147483647);
		ItemStack leftItem = inventory.getItem(0);
		ItemStack rightItem = inventory.getItem(1);
		if (leftItem != null && rightItem != null) {
			boolean leftIsBook = leftItem.getItemMeta() instanceof EnchantmentStorageMeta;
			boolean rightIsBook = rightItem.getItemMeta() instanceof EnchantmentStorageMeta;
			if (leftItem.getType() == rightItem.getType() || leftIsBook || rightIsBook) {
				Map<Enchantment, Integer> resultingEnchantments, addedEnchantments;
				Map<GoldenEnchant, Integer> resultingGoldenEnchantments, addedGoldenEnchantments;
				
				if (leftIsBook) {
					resultingEnchantments = new HashMap<>(
						((EnchantmentStorageMeta) leftItem.getItemMeta()).getStoredEnchants());
				} else {
					resultingEnchantments = new HashMap<>(leftItem.getItemMeta().getEnchants());
				}
				
				if (rightIsBook) {
					addedEnchantments = new HashMap<>(
						((EnchantmentStorageMeta) rightItem.getItemMeta()).getStoredEnchants());
				} else {
					addedEnchantments = rightItem.getItemMeta().getEnchants();
				}
				resultingGoldenEnchantments = EnchantManager.getItemGoldenEnchants(leftItem);
				addedGoldenEnchantments = EnchantManager.getItemGoldenEnchants(rightItem);
				
				for (Map.Entry<Enchantment, Integer> entry : addedEnchantments.entrySet()) {
					Enchantment rightEnchantment = entry.getKey();
					
					if (!rightEnchantment.canEnchantItem(leftItem) && !leftIsBook) {
						continue;
					}
					
					int rightEnchantmentLevel = ((Integer) entry.getValue()).intValue();
					if (!resultingEnchantments.containsKey(rightEnchantment)) {
						resultingEnchantments.put(rightEnchantment, Integer.valueOf(rightEnchantmentLevel));
						continue;
					}
					
					int leftEnchantmentLevel = ((Integer) resultingEnchantments.get(rightEnchantment)).intValue();
					if (leftEnchantmentLevel < rightEnchantmentLevel) {
						resultingEnchantments.put(rightEnchantment, Integer.valueOf(rightEnchantmentLevel));
						continue;
					}
					
					if (leftEnchantmentLevel == rightEnchantmentLevel) {
						int newLevel = rightEnchantmentLevel;
						if (isValidEnchantLevel(rightEnchantment, newLevel + 1))
							newLevel++;
						resultingEnchantments.put(rightEnchantment, Integer.valueOf(newLevel));
					}
				}
				
				for (Map.Entry<GoldenEnchant, Integer> entry : addedGoldenEnchantments.entrySet()) {
					GoldenEnchant rightEnchantment = entry.getKey();
					
					if(!EnchantManager.canEnchant(leftItem, rightEnchantment, (int) Integer.valueOf(entry.getValue())))
						continue;
					
					int rightEnchantmentLevel = ((Integer) entry.getValue()).intValue();
					if (!resultingGoldenEnchantments.containsKey(rightEnchantment)) {
						resultingGoldenEnchantments.put(rightEnchantment, Integer.valueOf(rightEnchantmentLevel));
						continue;
					}
					
					int leftEnchantmentLevel = ((Integer) resultingGoldenEnchantments.get(rightEnchantment)).intValue();
					if (leftEnchantmentLevel < rightEnchantmentLevel) {
						resultingGoldenEnchantments.put(rightEnchantment, Integer.valueOf(rightEnchantmentLevel));
						continue;
					}
					
					if (leftEnchantmentLevel == rightEnchantmentLevel) {
						int newLevel = rightEnchantmentLevel;
						if (EnchantManager.canEnchant(leftItem, rightEnchantment, newLevel + 1))
							newLevel++;
						resultingGoldenEnchantments.put(rightEnchantment, Integer.valueOf(newLevel));
					}
				}
				
				ItemStack resultItem = leftItem.clone();
				
				if (this.debug && event.getView().getPlayer().isOp()) {
					this.plugin
							.printToConsole(ChatColor.AQUA + "DEBUG INFO FOR " + event.getView().getPlayer().getName());
					for (Map.Entry<Enchantment, Integer> entry : resultingEnchantments.entrySet())
						this.plugin.printToConsole("Enchantment found: " + ChatColor.GREEN
								+ ((Enchantment) entry.getKey()).getKey().getKey());
				}
				
				if (leftIsBook) {
					EnchantmentStorageMeta resultItemMeta = (EnchantmentStorageMeta) resultItem.getItemMeta();
					Map<Enchantment, Integer> originalEnchantments = resultItemMeta.getStoredEnchants();
					for (Map.Entry<Enchantment, Integer> entry : originalEnchantments.entrySet())
						resultItemMeta.removeStoredEnchant(entry.getKey());
					for (Map.Entry<Enchantment, Integer> entry : resultingEnchantments.entrySet())
						resultItemMeta.addStoredEnchant(entry.getKey(), ((Integer) entry.getValue()).intValue(), true);
					resultItem.setItemMeta((ItemMeta) resultItemMeta);
					
				} else {
					for (Map.Entry<Enchantment, Integer> entry : (Iterable<Map.Entry<Enchantment, Integer>>) resultItem
							.getItemMeta().getEnchants().entrySet())
						resultItem.removeEnchantment(entry.getKey());
					resultItem.addUnsafeEnchantments(resultingEnchantments);
				}
				
				for (Map.Entry<GoldenEnchant, Integer> entry : resultingGoldenEnchantments.entrySet())
					EnchantManager.addEnchant(resultItem, entry.getKey(), (int) entry.getValue(), true);
				
				int repairCost = 0;
				
				if (leftIsBook) {
					for (Map.Entry<Enchantment, Integer> entry : resultingEnchantments.entrySet())
						repairCost += ((Integer) entry.getValue()).intValue();
				} else {
					for (Map.Entry<Enchantment, Integer> entry : resultingEnchantments.entrySet())
						repairCost += ((Integer) entry.getValue()).intValue();
					
					repairCost = repairCost * 2;
				}
				
				for (Map.Entry<GoldenEnchant, Integer> entry : resultingGoldenEnchantments.entrySet())
					repairCost += (int) entry.getValue();
				
				if (repairCost > 0)
					inventory.setRepairCost(repairCost);
				
				event.setResult(resultItem);
				
				int finalRepairCost = inventory.getRepairCost();
					
				event.getView().getPlayer().sendMessage(ChatColor.RED + "A jav�t�s �ra: " + ChatColor.GREEN + finalRepairCost + ChatColor.RED + " szint.");
				
				inventory.setRepairCost(finalRepairCost);
				event.getView().setProperty(InventoryView.Property.REPAIR_COST, finalRepairCost);
			}
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (!event.isCancelled()) {
			HumanEntity humanEntity = event.getWhoClicked();
			if (humanEntity instanceof Player) {
				Player player = (Player) humanEntity;
				if (event.getInventory() instanceof AnvilInventory) {
					AnvilInventory anvilInventory = (AnvilInventory) event.getInventory();
					InventoryView view = event.getView();
					int rawSlot = event.getRawSlot();
					if (rawSlot == view.convertSlot(rawSlot))
						if (rawSlot == 2) {
							ItemStack[] items = anvilInventory.getContents();
							if (items[0] != null && items[1] != null)
								if (event.getCurrentItem().getType() != Material.AIR
										&& event.getCurrentItem() != items[0] && event.getCurrentItem() != items[1])
									if (player.getLevel() >= anvilInventory.getRepairCost()) {
										int repairCost = anvilInventory.getRepairCost();
										int playerLevel = player.getLevel();
										int resultantLevel = playerLevel - repairCost;
										ItemStack itemToGive = event.getCurrentItem().clone();
										if (itemToGive.getAmount() > 1)
											itemToGive.setAmount(1);
										ItemStack returnedStack = items[0].clone();
										returnedStack.setAmount(returnedStack.getAmount() - 1);
										if (items[0].getAmount() > 1 && player.getInventory()
												.addItem(new ItemStack[] { returnedStack }).size() != 0)
											player.getWorld().dropItem(player.getLocation(), returnedStack);
										anvilInventory.remove(anvilInventory.getItem(0));
										anvilInventory.remove(anvilInventory.getItem(1));
										anvilInventory.remove(anvilInventory.getItem(2));
										if (player.getInventory().addItem(new ItemStack[] { itemToGive }).size() != 0)
											player.getWorld().dropItem(player.getLocation(), itemToGive);
										player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0F, 1.0F);
										player.giveExpLevels(resultantLevel - playerLevel);
									}
						}
				}
			}
		}
	}
	
	private boolean isValidEnchantLevel(Enchantment enchantment, int level) {
		if (level > enchantment.getMaxLevel())
			return false;
		return true;
	}
}
